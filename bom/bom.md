|       | References	  | Value	        | Footprint	                  | Quantity |
|-------|---------------|---------------|-----------------------------|----------|
| 1			| C2, C5, C6, C7, C8, C10	| 10uF	| C_0805_2012Metric	| 6
| 2			| C1	| 1uF	| C_0805_2012Metric	| 1
| 3			| C3	| 4.7uF	| C_0805_2012Metric	| 1
| 4			| C4	| 0.047uF	| C_0805_2012Metric	| 1
| 5			| C9	| 0.1uF	| C_0805_2012Metric	| 1
| 6			| R1, R7, R8, R12	| 470	| R_0805_2012Metric	| 4
| 7			| R2, R3, R4, R5	| 10k	| R_0805_2012Metric	| 4
| 8			| R6	| 1k	| R_0805_2012Metric	| 1
| 9			| R9	| 4.7k	| R_0805_2012Metric	| 1
| 10		|	R10	| 5.23k	| R_0805_2012Metric	| 1
| 11		|	R11	| 30.1k	| R_0805_2012Metric	| 1
| 12		|	L1	| 1uH	| L_Vishay_IHLP-2020	| 1
| 13		|	D1, D4	| TO-1608BC-MRE	| LED_0603_1608Metric	| 2
| 14		|	D2, D3	| TO-1608BC-PG	| LED_0603_1608Metric	| 2
| 15		|	U2	| TPS79933	| SOT-23-5	| 1
| 16		|	U1	| BQ25896	| WQFN-24-1EP_4x4mm_P0.5mm_EP2.7x2.7mm	| 1
| 17		|	SW1	| 1437566-3 (FSMSM)	| SW_SPST_FSMSM	| 1
| 18		|	J12	| MOLEX_22272021	| Molex_22272021_AE-6410-2_1x02-2.54mm	| 1
| 19		|	J5, J6, J7, J11	| PLS-2	| PinHeader_1x02_P2.54mm_Vertical	| 4
| 20		|	J3, J8, J9, J10, J13	| PLS-3	| PinHeader_1x03_P2.54mm_Vertical	| 5
| 21		|	J4	| PLS-9	| PinHeader_1x09_P2.54mm_Vertical	| 1
| 22		|	J1	| PBS-3	| PinSocket_1x03_P2.54mm_Vertical	| 1
| 23		|	J2	| PBS-9	| PinSocket_1x09_P2.54mm_Vertical	| 1
